# Utiliser une image de base compatible avec Raspberry Pi
FROM arm64v8/python:3.9-slim

# Définir le répertoire de travail
RUN mkdir /scrapsudoc
WORKDIR /scrapsudoc

# Install git and build tools
RUN apt-get update && apt-get install -y \
    git \
    gcc \
    build-essential \
    python3-dev

# Copier le fichier requirements.txt et installer les dépendances
COPY requirements.txt requirements.txt

# Install the dependencies
RUN pip install --no-cache-dir -r requirements.txt

# Télécharger les ressources NLTK
RUN python -m nltk.downloader punkt
RUN python -m nltk.downloader stopwords
RUN python -m nltk.downloader wordnet

# Copier le reste du code de l'application
COPY . .

# Ajouter le répertoire de travail au PYTHONPATH
ENV PYTHONPATH="${PYTHONPATH}:/scrapsudoc"

# Lister les fichiers pour debug
RUN ls -la /scrapsudoc
RUN ls -la /scrapsudoc/components

# Exposer le port sur lequel l'application s'exécute
EXPOSE 8050

# Commande pour démarrer l'application
CMD ["python", "index.py"]
