"""
    Master 2 Health Data Science 2022-2024
    Sujet de Webscrapping/IHM/ML - Djamel Zitouni, Wajdi Dhifli
    
    Récupération des données de Pépite - Université de Lille et les mémoires de Master 2
    d'ILIS

    Groupe composé de : 
    - Tracy LURANT
    - Malik MAMMAR
    - Hippolyte TOUZE
    - Antoine TESTON
    
    TO DO : 
    REQUIREMENT.TXT
    README.MD
    DOCSTRING des fonctions

    Fonctionnalités :
    Webscraping OK
    Interface DASH avec Dataviz OK
    NLP - BERT -> Comparaison de mots clés ? OK
    Réseaux / Kmeans ? Pas Kmeans mais réseau OK
    
    -------------------------------------------
    Script de configuration de l'app
    
"""


from dash import dcc, html, Dash, Input, Output
import dash_bootstrap_components as dbc
import dash_mantine_components as dmc
from dash import dash_table
import dash
from flask_caching import Cache
from flask import Flask
import gunicorn


external_stylesheets = [
    dbc.themes.FLATLY, dbc.icons.BOOTSTRAP 
]

app = Dash(__name__, external_stylesheets=external_stylesheets)
app.title="Pép(ai)te - Rapport des thèses et mémoires de l'Université de Lille"

server = app.server

cache = Cache(app.server, config={
    'CACHE_TYPE': 'filesystem',
    'CACHE_DIR': 'cache-directory',
    'CACHE_THRESHOLD': 200
})
app.config.suppress_callback_exceptions = True
