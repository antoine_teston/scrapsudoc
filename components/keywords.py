"""
    Master 2 Health Data Science 2022-2024
    Sujet de Webscrapping/IHM/ML - Djamel Zitouni, Wajdi Dhifli
    
    Récupération des données de Pépite - Université de Lille et les mémoires de Master 2
    d'ILIS

    Groupe composé de : 
    - Tracy LURANT
    - Malik MAMMAR
    - Hippolyte TOUZE
    - Antoine TESTON
    
    TO DO : 
    REQUIREMENT.TXT
    README.MD
    DOCSTRING des fonctions

    Fonctionnalités :
    Webscraping
    Interface DASH avec Dataviz
    NLP - BERT -> Comparaison de mots clés ?
    Réseaux / Kmeans ? 
    
    -------------------------------------------
    Script de la page principale du Dashboard
    
"""

# Import packages
import base64
import io
import os
import pandas as pd
from dash import Dash, html, dcc, Input, Output, State, dash_table
import dash_bootstrap_components as dbc
import dash_mantine_components as dmc
from dash.exceptions import PreventUpdate
import plotly.graph_objects as go
import plotly.express as px
from datetime import date, datetime
import string



# Dépendance des autres scripts
from app import app, server, cache
from components.functions import separate_keywords, process_keywords, generate_word_cloud

# Mise en page pour la page principale
keywords = dmc.Grid(
    style={"flex-grow": "1", "scroll": "auto"},
    children=[
        dmc.Col(
            html.Div([
                dmc.Title("Recherche par Mots-Clés", order=2, style={"textAlign": "center", "marginBottom": "20px"}),
                dmc.Text("Utilisez le menu déroulant ci-dessous pour sélectionner les mots-clés référencés dans les thèses et mémoires.", style={"textAlign": "center", "marginBottom": "20px", "fontSize": "16px"}),
                dcc.Dropdown(
                    id='options',
                    multi=True,
                    placeholder='Sélection des mots-clés référencés :'
                ),
                html.Div(style={"margin-bottom": "20px"}), # Séparation
                dcc.Loading(
                    id="loading-1",
                    type="circle",
                    children=dcc.Graph(id='fig4')
                ),
            ],
            style={"width": "100%", "height": "100%"},
            className="box-container"
            ),
            span=12,
        ),
        dmc.Col(
            html.Div([
                dmc.Title("Nuage de Mots des thématiques", order=4, style={"textAlign": "center", "marginBottom": "20px"}),
                dcc.Loading(
                    id="loading-1",
                    type="circle",
                    children=html.Img(id='wordcloud', style={"max-width": "100%", "max-height": "100%"})
                ),
            ],
            style={"width": "100%", "height": "100%"},
            className="box-container"
            ),
            span=6,
        ),
        dmc.Col(
            html.Div([
                dmc.Title("Section en Construction", order=4, style={"textAlign": "center", "marginBottom": "20px"}),
                dmc.Text("Cette section est en cours de construction. Revenez bientôt pour plus d'informations.", style={"textAlign": "center", "fontSize": "16px"})
            ],
            style={"width": "100%", "height": "100%"},
            className="box-container"
            ),
            span=6,
        )
    ]
)

@cache.memoize(timeout=60*5)
def get_processed_data(df, cache_dir='data'):
    """
    Processes the DataFrame and returns keyword counts grouped by year.
    Args:
        df (pd.DataFrame): DataFrame containing the raw data.
    Returns:
        pd.DataFrame: DataFrame with 'year', 'french_keywords', and 'count'.
    """
    all_keywords = process_keywords(df, cache_dir)
    df_keywords = pd.DataFrame(all_keywords)

    # Ensure 'year' column exists before attempting to drop NaNs
    if 'year' in df_keywords.columns:
        df_keywords = df_keywords.dropna(subset=['year'])
    
    if df_keywords.empty or not {'french_keywords', 'year'}.issubset(df_keywords.columns):
        return pd.DataFrame()

    keyword_counts = df_keywords.groupby(['year', 'french_keywords']).size().reset_index(name='count')
    pairs_df = df_keywords[['director', 'french_keywords']]
    return keyword_counts

# Combined callback to update dropdown options and the graph based on selected keywords
@app.callback(
    [Output("options", "options"),
     Output('fig4', 'figure'),
    Output('wordcloud', 'src')],
    [Input('step', 'data'),
     Input('options', 'value')]
)
def update_dropdown_and_graph(data, selected_keywords):
    """
    Updates the dropdown options and the graph based on the processed data.
    Args:
        data (dict): Dictionary containing the raw data.
        selected_keywords (list): List of selected keywords.
    Returns:
        tuple: Dropdown options and the updated figure.
    """
    if "df2" not in data:
        raise PreventUpdate

    df = pd.DataFrame(data["df2"])
    
    processed_data = get_processed_data(df)
    
    
    if processed_data.empty or not {'french_keywords', 'year', 'count'}.issubset(processed_data.columns):
        return [], go.Figure()
    
    # Update dropdown options
    keywords = processed_data['french_keywords'].unique()
    options = [{'label': kw, 'value': kw} for kw in keywords]
    top_keywords = processed_data.groupby('french_keywords')['count'].sum().nlargest(10).index.tolist()
    
    # Prepare the data for the stacked area plot
    if selected_keywords:
        filtered_data = processed_data[processed_data['french_keywords'].isin(selected_keywords)]
    else:
        filtered_data = processed_data[processed_data['french_keywords'].isin(top_keywords)]
    
    # Define a custom colorscale
    custom_colorscale = px.colors.qualitative.Prism
    
    fig4 = px.area(filtered_data, x='year', y='count', color='french_keywords',
                  labels={'french_keywords': 'Mots-clés', 'count': 'Occurrences', 'year': 'Année'},
                  title='Mots-clés les plus fréquents',
                  line_group='french_keywords', hover_data={'french_keywords': True},
                  color_discrete_sequence=custom_colorscale)
    
    fig4.update_traces(mode='lines+markers', hovertemplate='%{y}')
    fig4.update_layout(
        xaxis=dict(tickmode='linear'),
        yaxis=dict(showgrid=True, gridcolor='lightgrey'),
        plot_bgcolor='white',
        hovermode='x unified',
        legend=dict(title='Mots-clés', orientation='v', x=1.02, y=1, bgcolor='rgba(255, 255, 255, 0.5)'),
        margin=dict(l=40, r=40, t=40, b=40)
    )
    
    wordcloud = generate_word_cloud(options)  
    
    return options, fig4, wordcloud