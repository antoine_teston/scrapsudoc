"""
    Master 2 Health Data Science 2022-2024
    Sujet de Webscrapping/IHM/ML - Djamel Zitouni, Wajdi Dhifli
    
    Récupération des données de Pépite - Université de Lille et les mémoires de Master 2
    d'ILIS

    Groupe composé de : 
    - Tracy LURANT
    - Malik MAMMAR
    - Hippolyte TOUZE
    - Antoine TESTON
    
    TO DO : 
    REQUIREMENT.TXT
    README.MD
    DOCSTRING des fonctions

    Fonctionnalités :
    Webscraping
    Interface DASH avec Dataviz
    NLP - BERT -> Comparaison de mots clés ?
    Réseaux / Kmeans ? 
    
    -------------------------------------------
    Script de la page principale du Dashboard
    
"""

# Import packages
import os
import pandas as pd
from dash import Dash, html, dcc, Input, Output, State, dash_table
import dash_bootstrap_components as dbc
import dash_mantine_components as dmc
from dash.exceptions import PreventUpdate
import plotly.graph_objects as go
import plotly.express as px
from datetime import date, datetime

# Dépendance des autres scripts
import components.functions as functions
import components.graphique as graphique

# Layout for main page
mainpage = dmc.Grid(
            style={"flex-grow": "1"},
            children=[
                dmc.Col(
                    html.Div(
                        children=[
                            dmc.Title("Pép(AI)te - Rapport des thèses et mémoires", order=1, style={"textAlign": "center", "marginBottom": "20px"}),
                            dmc.Text("Faculté d'ingénierie et de Management de la Santé - Université de Lille", style={"textAlign": "center", "marginBottom": "20px", "fontSize": "18px"}),
                            dmc.Text("Ce rapport présente une analyse complète des thèses et mémoires soutenus à la Faculté d'ingénierie et de Management de la Santé. Utilisez les onglets pour afficher les différentes données", style={"textAlign": "center", "marginBottom": "20px", "fontSize": "16px"}),
                            html.Img(src="assets/logo_pepite.png", style={"display": "block", "margin": "0 auto", "width": "200px", "height": "auto"}),
                        ],
                        style={
                            "padding": "20px",
                            "borderRadius": "10px",
                            "boxShadow": "0 4px 8px 0 rgba(0, 0, 0, 0.2)",
                            "backgroundColor": "#f9f9f9"
                        },
                        className="box-container",
                    ),
                    span=12,
                ),
                dmc.Col(
                    html.Div(
                        dcc.Graph(id="fig"),
                        style={"width": "100%", "height": "100%"},
                        className="box-container",
                    ),
                    span=6,
                ),
                dmc.Col(
                    html.Div(
                        dcc.Graph(id="fig2"),
                        style={"width": "100%", "height": "100%"},
                        className="box-container",
                    ),
                    span=6,
                ),
                dmc.Col(
                    html.Div(
                        dcc.Graph(id="fig3", style={"width": "auto", "height": "100%"}),
                        style={"width": "100%", "height": "auto", "margin": "0 auto"},
                        className="box-container",
                    ),
                    span=12
                )
            ]
        )
