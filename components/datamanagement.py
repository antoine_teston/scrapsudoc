"""
    Master 2 Health Data Science 2022-2024
    Sujet de Webscrapping/IHM/ML - Djamel Zitouni, Wajdi Dhifli
    
    Récupération des données de Pépite - Université de Lille et les mémoires de Master 2
    d'ILIS

    Groupe composé de : 
    - Tracy LURANT
    - Malik MAMMAR
    - Hippolyte TOUZE
    - Antoine TESTON
    
    TO DO : 
    REQUIREMENT.TXT
    README.MD
    DOCSTRING des fonctions

    Fonctionnalités :
    Webscraping
    Interface DASH avec Dataviz
    NLP - BERT -> Comparaison de mots clés ?
    Réseaux / Kmeans ? 
    
    -------------------------------------------
    Script de datamanagement de la dataframe .csv récupérée par le webscrapping dans server.py
    
"""

# Importations des packages
import pandas as pd
import re
import os
from dash import Output, Input, State, html

from components.functions import process_names, update_parcours, update_dates
from components.functions import separate_keywords
import server

from app import app


@app.callback(
    Output("step", "data"),
    Input("memory", "data")
)

def data_management(data):
    """
    Applique les transformations nécessaires au DataFrame.

    Args:
    df (pd.DataFrame): Le DataFrame à transformer.

    Returns:
    pd.DataFrame: Le DataFrame transformé.
    """
    
    if "df" in data:
        df = pd.DataFrame(data["df"])
    # Colonnes à traiter pour l'inversion des noms
    columns_to_process = ['authors', 'director', 'jury']
    
    # Appliquer la fonction aux colonnes spécifiées
    for column in columns_to_process:
        df[column] = df[column].apply(lambda x: ' ; '.join([process_names(name) for name in x.split(' ; ')]))
    
    # Appliquer la fonction à chaque ligne
    df = df.apply(update_parcours, axis=1)
    
    # Appliquer la fonction à la colonne 'date'
    df['date'] = df['date'].apply(update_dates)
    
    return {
            "df2": df.to_dict("records")
            }

