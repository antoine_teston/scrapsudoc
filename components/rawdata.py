"""
    Master 2 Health Data Science 2022-2024
    Sujet de Webscrapping/IHM/ML - Djamel Zitouni, Wajdi Dhifli
    
    Récupération des données de Pépite - Université de Lille et les mémoires de Master 2
    d'ILIS

    Groupe composé de : 
    - Tracy LURANT
    - Malik MAMMAR
    - Hippolyte TOUZE
    - Antoine TESTON
    
    TO DO : 
    REQUIREMENT.TXT
    README.MD
    DOCSTRING des fonctions

    Fonctionnalités :
    Webscraping
    Interface DASH avec Dataviz
    NLP - BERT -> Comparaison de mots clés ?
    Réseaux / Kmeans ? 
    
    -------------------------------------------
    Script de la page du tableau des données brutes avec fonctions avancées de recherche
    
"""

# Import packages
import os
import pandas as pd
from pandas import json_normalize
from dash import Dash, html, dash_table, dcc, Input, Output, State
import dash_bootstrap_components as dbc
import dash_mantine_components as dmc
from dash.exceptions import PreventUpdate
import plotly.graph_objects as go
import plotly.express as px
from datetime import date, datetime
import unicodedata


# Dépendance des autres scripts
from app import app, server
import components.datamanagement as datamanagement
import components.functions as functions

def remove_accents(input_str):
    """
    Supprime les accents d'une chaîne de caractères.
    
    Args:
        input_str (str): La chaîne de caractères à traiter.
    
    Returns:
        str: La chaîne de caractères sans accents.
    """
    if input_str is None:
        return ""
    nfkd_form = unicodedata.normalize('NFKD', input_str)
    return "".join([c for c in nfkd_form if not unicodedata.combining(c)])

rawdata = dmc.Grid(
    style={"flex-grow": "1"},
    children=[
        dmc.Col(
            html.Div(id="search-container", 
                children=[
                    dmc.Title("Recherche dans les Mémoires", order=2, style={'textAlign': 'center', 'marginBottom': '20px'}),
                    dmc.Text(
                        "Utilisez la recherche globale pour trouver des mémoires spécifiques ou utilisez la recherche avancée pour affiner les résultats.",
                        style={'textAlign': 'center', 'marginBottom': '20px'}
                    ),
                    dmc.TextInput(
                        id='search-input',
                        type='text',
                        placeholder='Recherche globale dans les mémoires..',
                        style={'width': '100%', 'margin': '10px 0', 'fontSize': '16px', 'padding': '10px'}
                    ),
                    dmc.Accordion(
                        children=[
                            dmc.AccordionItem(
                                value="advanced-search",
                                children=[
                                    dmc.AccordionControl("Recherche Avancée", style={'width': '100%', 'textAlign': 'left'}),
                                    dmc.AccordionPanel(
                                        children=[
                                            html.Div(
                                                children=[
                                                    dmc.TextInput(id='filter-title', type='text', placeholder='Filtrer par titre', style={'margin': '5px', 'fontSize': '12px', 'flex': '1'}),
                                                    dmc.TextInput(id='filter-authors', type='text', placeholder='Filtrer par auteurs', style={'margin': '5px', 'fontSize': '12px', 'flex': '1'}),
                                                    dmc.TextInput(id='filter-keywords', type='text', placeholder='Filtrer par mots-clés', style={'margin': '5px', 'fontSize': '12px', 'flex': '1'}),
                                                    dmc.TextInput(id='filter-speciality', type='text', placeholder='Filtrer par spécialité', style={'margin': '5px', 'fontSize': '12px', 'flex': '1'}),
                                                    dmc.TextInput(id='filter-director', type='text', placeholder='Filtrer par directeur', style={'margin': '5px', 'fontSize': '12px', 'flex': '1'}),
                                                    dmc.TextInput(id='filter-jury', type='text', placeholder='Filtrer par jury', style={'margin': '5px', 'fontSize': '12px', 'flex': '1'}),
                                                    dmc.TextInput(id='filter-date', type='text', placeholder='Filtrer par date', style={'margin': '5px', 'fontSize': '12px', 'flex': '1'}),
                                                    dmc.TextInput(id='filter-parcours', type='text', placeholder='Filtrer par parcours', style={'margin': '5px', 'fontSize': '12px', 'flex': '1'})
                                                ],
                                                style={'display': 'flex', 'flexWrap': 'wrap', 'gap': '10px', 'width': '100%'}
                                            )
                                        ]
                                    )
                                ]
                            )
                        ]
                    )
                ],
                style={'display': 'flex', 'flexDirection': 'column', 'alignItems': 'center'},
                className="box-container",
            ),
            span=12
        ),
        dmc.Col(
            html.Div(id="table-container", 
                    style={'height': 'calc(100vh - 200px)', 'overflow': 'auto'},
                    className="box-container",
            ),
            span=12,
        )
    ]
)





@app.callback(
    Output("table-container", "children"),
    Input("step", "data"),
    Input("search-input", "value"),
    Input("filter-title", "value"),
    Input("filter-authors", "value"),
    Input("filter-keywords", "value"),
    Input("filter-director", "value"),
    Input("filter-jury", "value"),
    Input("filter-date", "value"),
    Input("filter-parcours", "value")
)
def generate_table(data, search_value, title_filter, authors_filter, keywords_filter, director_filter, jury_filter, date_filter, parcours_filter):
    """
    Génère un tableau interactif à partir d'un DataFrame pandas avec recherche et pagination.
    
    Args:
    data (dict): Les données contenant le DataFrame à convertir en tableau interactif.
    search_value (str): La valeur de recherche pour filtrer les données.
    title_filter (str): Le filtre pour la colonne titre.
    authors_filter (str): Le filtre pour la colonne auteurs.
    keywords_filter (str): Le filtre pour la colonne mots-clés.
    speciality_filter (str): Le filtre pour la colonne spécialité.
    director_filter (str): Le filtre pour la colonne directeur.
    jury_filter (str): Le filtre pour la colonne jury.
    date_filter (str): Le filtre pour la colonne date.
    parcours_filter (str): Le filtre pour la colonne parcours.

    Returns:
    html.Div: Contenant le tableau interactif.
    """
    if "df2" in data:
        df2 = pd.DataFrame(data["df2"])

        # Filtrer les données en fonction de la valeur de recherche globale
        if search_value:
            search_value = remove_accents(search_value.lower())
            df2 = df2[df2.apply(lambda row: row.astype(str).apply(remove_accents).str.contains(search_value, case=False).any(), axis=1)]

        # Filtrer par colonne
        if title_filter:
            title_filter = remove_accents(title_filter.lower())
            df2 = df2[df2['title'].apply(remove_accents).str.contains(title_filter, case=False)]
        if authors_filter:
            authors_filter = remove_accents(authors_filter.lower())
            df2 = df2[df2['authors'].apply(remove_accents).str.contains(authors_filter, case=False)]
        if keywords_filter:
            keywords_filter = remove_accents(keywords_filter.lower())
            df2 = df2[df2['keywords'].apply(remove_accents).str.contains(keywords_filter, case=False)]
        if director_filter:
            director_filter = remove_accents(director_filter.lower())
            df2 = df2[df2['director'].apply(remove_accents).str.contains(director_filter, case=False)]
        if jury_filter:
            jury_filter = remove_accents(jury_filter.lower())
            df2 = df2[df2['jury'].apply(remove_accents).str.contains(jury_filter, case=False)]
        if date_filter:
            date_filter = remove_accents(date_filter.lower())
            df2 = df2[df2['date'].apply(remove_accents).str.contains(date_filter, case=False)]
        if parcours_filter:
            parcours_filter = remove_accents(parcours_filter.lower())
            df2 = df2[df2['parcours'].apply(remove_accents).str.contains(parcours_filter, case=False)]

        table = dash_table.DataTable(
            id='table',
            data=df2.to_dict('records'),
            columns=[{"name": i, "id": i} for i in df2.columns],
            page_size=10,  # Nombre de lignes par page
            style_table={
                'height': '100%',
                'overflowY': 'auto'
            },
            style_cell={
                'textAlign': 'left',
                'whiteSpace': 'normal',
                'height': 'auto',
                'minWidth': '120px',
                'width': '120px',
                'maxWidth': '200px',
                'overflow': 'hidden',
                'textOverflow': 'ellipsis',
                'fontFamily': 'Arial, sans-serif',
                'fontSize': '16px'
            },
            style_header={
                'backgroundColor': 'rgb(230, 230, 230)',
                'fontWeight': 'bold',
                'fontFamily': 'Arial, sans-serif',
                'fontSize': '10px'
            },
            filter_action="none",  # Désactiver la recherche native
            sort_action="native",  # Activer le tri
            page_action="native",  # Activer la pagination
            style_data_conditional=[
                {
                    'if': {'row_index': 'odd'},
                    'backgroundColor': 'rgb(248, 248, 248)',
                    'fontFamily': 'Arial, sans-serif',
                    'fontSize': '16px'
                }
            ]
        )
        return html.Div(table, style={'height': '100%'})  # Hauteur maximale du tableau
        
    raise PreventUpdate