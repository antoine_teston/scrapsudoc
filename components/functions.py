"""
    Master 2 Health Data Science 2022-2024
    Sujet de Webscrapping/IHM/ML - Djamel Zitouni, Wajdi Dhifli
    
    Récupération des données de Pépite - Université de Lille et les mémoires de Master 2
    d'ILIS

    Groupe composé de : 
    - Tracy LURANT
    - Malik MAMMAR
    - Hippolyte TOUZE
    - Antoine TESTON
    
    TO DO : 
    REQUIREMENT.TXT
    README.MD
    DOCSTRING des fonctions

    Fonctionnalités :
    Webscraping
    Interface DASH avec Dataviz
    NLP - BERT -> Comparaison de mots clés ?
    Réseaux / Kmeans ? 
    
    -------------------------------------------
    Script de dictionnaire des fonctions de la webapp
    
"""

# Chargement des packages
import pandas as pd
import io
import base64
import re
from datetime import datetime
import os
import unicodedata
from langdetect import detect, LangDetectException
import nltk
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer, PorterStemmer
import string
from sklearn.feature_extraction.text import TfidfVectorizer
import matplotlib.pyplot as plt
from wordcloud import WordCloud
import json

def process_names(name):
    """
    Traite les noms en inversant les parties de noms séparées par une virgule.
    
    Args:
        name (str): Le nom à traiter.
    
    Returns:
        str: Le nom traité.
    """
    parts = name.split(',')
    parts = [part.strip() for part in parts]
    if len(parts) > 1:
        first, last = parts[0], parts[-1]
        middle = parts[1:-1]
        parts = [last] + middle + [first]
    return ' '.join(parts)

def update_parcours(row):
    """
    Met à jour la colonne 'parcours' d'une ligne du DataFrame en fonction de la colonne 'speciality'.
    
    Args:
        row (pd.Series): La ligne du DataFrame à traiter.
    
    Returns:
        pd.Series: La ligne mise à jour.
    """
    parts = [part.strip() for part in row['speciality'].split(';')]
    if len(parts) > 1:
        row['speciality'] = parts[0]
        if not row['parcours'] or row['parcours'] == "No parcours found":
            row['parcours'] = parts[1]
    return row

def update_dates(date_str):
    """
    Met à jour le format des dates si elles correspondent à un certain modèle.
    
    Args:
        date_str (str): La date à traiter.
    
    Returns:
        str: La date mise à jour.
    """
    if re.match(r'^//\d{4}$', date_str):
        return f'01/01/{date_str[2:]}'
    return date_str

def preprocess_keyword_for_detection(keyword):
    """
    Pré-traite un mot-clé en le mettant en minuscule et en supprimant la ponctuation.
    
    Args:
        keyword (str): Le mot-clé à pré-traiter.
    
    Returns:
        str: Le mot-clé pré-traité.
    """
    keyword = keyword.lower()
    keyword = keyword.translate(str.maketrans('', '', string.punctuation))
    return keyword

def detect_language(keyword):
    """
    Détecte la langue d'un mot-clé.
    
    Args:
        keyword (str): Le mot-clé dont la langue doit être détectée.
    
    Returns:
        str: La langue détectée ('fr', 'en' ou 'unknown').
    """
    try:
        return detect(keyword)
    except LangDetectException:
        return 'unknown'

def ensure_french_or_english(lang, keyword):
    """
    Assure que la langue détectée est le français ou l'anglais, sinon essaie de redétecter.
    
    Args:
        lang (str): La langue détectée initialement.
        keyword (str): Le mot-clé à vérifier.
    
    Returns:
        str: La langue vérifiée ('fr' ou 'en').
    """
    if lang in ['fr', 'en']:
        return lang
    else:
        try:
            simplified_keyword = preprocess_keyword_for_detection(keyword)
            simplified_lang = detect(simplified_keyword)
            if simplified_lang in ['fr', 'en']:
                return simplified_lang
        except LangDetectException:
            pass
        return 'en'

def separate_keywords(row):
    """
    Sépare les mots-clés en mots-clés français et anglais.
    
    Args:
        row (str): La chaîne de mots-clés à séparer.
    
    Returns:
        dict: Un dictionnaire contenant les mots-clés séparés.
    """
    french_keywords = []
    english_keywords = []
    
    if pd.isna(row):
        return "", ""
    
    keywords = row.split(';')
    for keyword in keywords:
        original_keyword = keyword.strip()
        keyword_processed = preprocess_keyword_for_detection(original_keyword)
        
        lang = detect_language(keyword_processed)
        lang = ensure_french_or_english(lang, original_keyword)
        
        if lang == 'fr':
            french_keywords.append(original_keyword)
        elif lang == 'en':
            english_keywords.append(original_keyword)
    
    return {"french_keywords": "; ".join(french_keywords), "english_keywords": "; ".join(english_keywords)}

def process_keywords(df, cache_dir='data'):
    """
    Traite les mots-clés dans le DataFrame et sauvegarde les résultats dans un fichier cache.
    
    Args:
        df (pd.DataFrame): Le DataFrame contenant les données brutes.
        cache_dir (str): Le répertoire où sauvegarder le fichier cache.
    
    Returns:
        list: Une liste de mots-clés traités.
    """
    if 'keywords' not in df.columns or 'date' not in df.columns:
        raise ValueError("Le DataFrame doit contenir les colonnes 'keywords' et 'date'.")
    
    cache_file = os.path.join(cache_dir, 'processed_keywords.json')
    
    if os.path.exists(cache_file):
        with open(cache_file, 'r') as f:
            all_keywords = json.load(f)
        return all_keywords
    
    keyword_df = df['keywords'].apply(separate_keywords).apply(pd.Series)
    df = pd.concat([df, keyword_df], axis=1)
    df['year'] = pd.to_datetime(df['date'], errors='coerce').dt.year
    
    all_keywords = []
    for _, row in df.iterrows():
        year = row['year']
        director = row['director']
        keywords = row['french_keywords']
        if pd.notna(keywords):
            for keyword in keywords.split(';'):
                all_keywords.append({'french_keywords': keyword.strip(), 'year': year, 'director': director})
    
    if not os.path.exists(cache_dir):
        os.makedirs(cache_dir)
    
    with open(cache_file, 'w') as f:
        json.dump(all_keywords, f)

    return all_keywords

def get_last_modified_date(file_path):
    """
    Retourne la date de la dernière modification du fichier spécifié.
    
    Args:
        file_path (str): Le chemin du fichier.
    
    Returns:
        str: La date de la dernière modification au format 'dd/mm/YYYY'.
    """
    if os.path.isfile(file_path):
        timestamp = os.path.getmtime(file_path)
        return datetime.fromtimestamp(timestamp).strftime('%d/%m/%Y')
    return "File not found"

def needs_update(last_update_str):
    """
    Vérifie si une mise à jour est nécessaire en comparant la date de la dernière mise à jour avec la date actuelle.
    
    Args:
        last_update_str (str): La date de la dernière mise à jour au format 'dd/mm/YYYY'.
    
    Returns:
        bool: True si une mise à jour est nécessaire, False sinon.
    """
    if last_update_str == "No data":
        return True
    last_update_date = datetime.strptime(last_update_str, '%d/%m/%Y')
    return last_update_date.date() < datetime.today().date()

def load_initial_data():
    """
    Charge les données initiales à partir d'un fichier CSV et retourne un DataFrame ainsi que les informations de mise à jour.
    
    Returns:
        tuple: Un tuple contenant le DataFrame sous forme de dictionnaire, le log de sortie et la date de la dernière mise à jour.
    """
    if os.path.isfile('./data/pepite_data.csv'):
        articles_df = pd.read_csv('./data/pepite_data.csv')
        last_update = get_last_modified_date('./data/pepite_data.csv')
        log_output = f"Loaded data from CSV (Last updated: {last_update})"
        return {"df": articles_df.to_dict("records")}, log_output, f"Dernière MAJ : {last_update}"
    return {}, "", "No data"

def generate_word_cloud(options):
    """
    Génère un nuage de mots à partir des options fournies.
    
    Args:
        options (list): Liste de dictionnaires contenant les valeurs des mots-clés.
    
    Returns:
        str: L'image du nuage de mots encodée en base64.
    """
    values = ", ".join(item['value'] for item in options if item['value'])
    review_text = values.strip().lower()
    review_text = review_text.translate(str.maketrans('', '', string.punctuation))
    tokens = word_tokenize(review_text)
    stop_words = set(stopwords.words('french'))
    tokens = [word for word in tokens if word not in stop_words]
    porter = PorterStemmer()
    stemmed_tokens = [porter.stem(word) for word in tokens]
    lemmatizer = WordNetLemmatizer()
    lemmatized_words = [lemmatizer.lemmatize(word) for word in stemmed_tokens]
    cleaned_text = ' '.join(lemmatized_words)
    vectorizer = TfidfVectorizer(ngram_range=(1, 2))
    tfidf_matrix = vectorizer.fit_transform([cleaned_text])
    feature_names = vectorizer.get_feature_names_out()
    tfidf_scores = tfidf_matrix.toarray()[0]
    sorted_indices = tfidf_scores.argsort()[::-1]
    N = 9
    top_keywords = [(feature_names[idx], tfidf_scores[idx]) for idx in sorted_indices[:N]]
    top_keywords_dict = {keyword: score for keyword, score in top_keywords}
    wordcloud = WordCloud(
        background_color='white',
        colormap='prism',
        max_font_size=100,
        min_font_size=10,
        max_words=200,
        contour_color='steelblue',
        contour_width=2,
        random_state=42
        ).generate_from_frequencies(top_keywords_dict)
    
    img = io.BytesIO()
    wordcloud.to_image().save(img, format='PNG')
    img.seek(0)
    
    return 'data:image/png;base64,{}'.format(base64.b64encode(img.getvalue()).decode())

def save_graph_elements(elements, cache_dir='data', filename='graph_elements.json'):
    """
    Sauvegarde les éléments du graphique dans un fichier cache.
    
    Args:
        elements (list): Liste des éléments du graphique.
        cache_dir (str): Répertoire où sauvegarder le fichier cache.
        filename (str): Nom du fichier cache.
    """
    if not os.path.exists(cache_dir):
        os.makedirs(cache_dir)
    cache_file = os.path.join(cache_dir, filename)
    with open(cache_file, 'w') as f:
        json.dump(elements, f)

def load_graph_elements(cache_dir='data', filename='graph_elements.json'):
    """
    Charge les éléments du graphique depuis un fichier cache.
    
    Args:
        cache_dir (str): Répertoire où se trouve le fichier cache.
        filename (str): Nom du fichier cache.
    
    Returns:
        list: Liste des éléments du graphique.
    """
    cache_file = os.path.join(cache_dir, filename)
    if os.path.exists(cache_file):
        with open(cache_file, 'r') as f:
            elements = json.load(f)
        return elements
    return None
