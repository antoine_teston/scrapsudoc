"""
    Master 2 Health Data Science 2022-2024
    Sujet de Webscrapping/IHM/ML - Djamel Zitouni, Wajdi Dhifli
    
    Récupération des données de Pépite - Université de Lille et les mémoires de Master 2
    d'ILIS

    Groupe composé de : 
    - Tracy LURANT
    - Malik MAMMAR
    - Hippolyte TOUZE
    - Antoine TESTON
    
    TO DO : 
    REQUIREMENT.TXT
    README.MD
    DOCSTRING des fonctions

    Fonctionnalités :
    Webscraping OK
    Interface DASH avec Dataviz OK
    NLP - BERT -> Comparaison de mots clés ? OK
    Réseaux / Kmeans ? Pas Kmeans mais réseau OK
    
    -------------------------------------------
    Script des sidebars de l'application
    
"""

# Import packages
import os
import pandas as pd
from dash import Dash, html, dcc, Input, Output, State, callback_context
import dash_bootstrap_components as dbc
import dash_mantine_components as dmc
from dash.exceptions import PreventUpdate
from datetime import datetime

from app import app
from server import fetch_titles_from_pepite
from components.functions import get_last_modified_date

# Sidebar content
sidebar = html.Div(
    id="mainSidebar",
    className="sidebar",
    children=[
        html.Div(
            className="sidebar-header",
            children=[
                html.Img(src='/assets/logo_pepite.png'),  # Chemin vers l'image
            ]
        ),                
        dbc.Nav(
            [
                dbc.NavLink([html.I(className="bi bi-speedometer2 me-3"), " Dashboard"], href="/", active="exact"),
                dbc.NavLink([html.I(className="bi bi-people me-3"), " Keywords"], href="/keywords", active="exact"),
                dbc.NavLink([html.I(className="bi bi-share me-3"), " Network"], href="/network", active="exact"),
                dbc.NavLink([html.I(className="bi bi-table me-3"), " Table"], href="/rawdata", active="exact"),
            ],
            vertical=True,
            pills=True,
            className="menu-content"
        ),
        html.Div(
            className="menu-content",
            children=[
                dbc.Alert(id="last-update", color="info", className="menu-content"),  # Updated to use id
                dbc.Button("Mise à jour", id="load-data-btn", color="info", className="menu-content", disabled=True),
            ],
            style={"margin-top": "auto"},  # Push to the bottom
        ),
        html.Div(
            className="user-info",
            children=[
                html.A(html.I(className="bi bi-git"), href="https://gitlab.com", className="ms-3", target="_blank")
            ]
        )
    ],
)

mini_sidebar = html.Div(
    id="miniSidebar",
    className="mini-sidebar",
    children=[
        html.Div(
            className="mini-sidebar-header",
            children=[
                html.Img(src='/assets/mini_logo_pepite.png'),
                html.Hr(),  # Chemin vers l'image
                html.Button(html.I(className="bi bi-list"), id="openBtn", className="open-btn"),
            ]
        ),
        html.Div(
            className="menu-content",
            children=[
                dbc.NavLink(html.I(className="bi bi-speedometer2"), href="/", active="exact"),
                html.Hr(),
                dbc.NavLink(html.I(className="bi bi-people"), href="/keywords", active="exact"),
                html.Hr(),
                dbc.NavLink(html.I(className="bi bi-share"), href="/network", active="exact"),
                html.Hr(),
                dbc.NavLink(html.I(className="bi bi-table"), href="/rawdata", active="exact"),
            ],
        )
    ],
    style={'display': 'none'}
)
@app.callback(
    Output("log-output", "children"),
    Output("memory", "data"),
    Output("last-update", "children"),
    Output('interval-component', 'disabled'),
    Input("interval-component", "n_intervals"),
    prevent_initial_call=False
)
def load_initial_data(n_intervals):
    logs = []
    csv_path = './data/pepite_data.csv'

    if os.path.isfile(csv_path):
        articles_df = pd.read_csv(csv_path)
        last_update = get_last_modified_date(csv_path)
        logs.append(f"Loaded data from CSV (Last updated: {last_update})")
        initial_data = {"df": articles_df.to_dict("records"), "last_update": last_update}
    else:
        articles_df = pd.DataFrame()
        last_update = "No data"
        logs.append("No initial data found.")
        initial_data = {"df": articles_df.to_dict("records"), "last_update": last_update}

    return "\n".join(logs), initial_data, f"Dernière MAJ : {last_update}", True



@app.callback(
    Output('mainSidebar', 'className'),
    Output('mainContent', 'style'),
    Output('miniSidebar', 'style'),
    Input('mainContent', 'n_clicks'),
    Input('openBtn', 'n_clicks'),
    State('mainSidebar', 'className'),
    State('mainContent', 'style'),
    State('miniSidebar', 'style')
)
def toggle_sidebars(content_click, open_n, sidebar_class, content_style, mini_sidebar_style):
    ctx = callback_context

    if not ctx.triggered:
        return sidebar_class, content_style, mini_sidebar_style

    button_id = ctx.triggered[0]['prop_id'].split('.')[0]

    if button_id == 'mainContent':
        if 'closed' not in sidebar_class:
            sidebar_class += ' closed'
            content_style['margin-left'] = '5%'
            content_style['width'] = '95%'
            mini_sidebar_style['display'] = 'block'
    elif button_id == 'openBtn':
        sidebar_class = sidebar_class.replace(' closed', '')
        content_style['margin-left'] = '16.67%'
        content_style['width'] = '83.33%'
        mini_sidebar_style['display'] = 'none'

    return sidebar_class, content_style, mini_sidebar_style
