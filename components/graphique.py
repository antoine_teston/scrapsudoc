"""
    Master 2 Health Data Science 2022-2024
    Sujet de Webscrapping/IHM/ML - Djamel Zitouni, Wajdi Dhifli
    
    Récupération des données de Pépite - Université de Lille et les mémoires de Master 2
    d'ILIS

    Groupe composé de : 
    - Tracy LURANT
    - Malik MAMMAR
    - Hippolyte TOUZE
    - Antoine TESTON
    
    TO DO : 
    REQUIREMENT.TXT
    README.MD
    DOCSTRING des fonctions

    Fonctionnalités :
    Webscraping
    Interface DASH avec Dataviz
    NLP - BERT -> Comparaison de mots clés ?
    Réseaux / Kmeans ? 
    
    -------------------------------------------
    Script de développement des graphes de la page principale
    
"""


#### Import Libraries ####

import pandas as pd
import plotly.graph_objects as go
from dash import Output, Input, State, html
import dash_mantine_components as dmc
from app import app
from components.datamanagement import data_management
import server

@app.callback(
    [Output("fig", "figure"),
     Output('fig2', 'figure'),
     Output('fig3', 'figure')],
    Input("step", "data")
)
def datamanagement_graph(data):
    if "df2" not in data:
        raise dash.exceptions.PreventUpdate

    df = pd.DataFrame(data["df2"])

    # Convertir les dates au format datetime
    df['date'] = pd.to_datetime(df['date'], format='%d/%m/%Y', dayfirst=True)

    # Extraire l'année de la date
    df['year'] = df['date'].dt.year

    # Compter le nombre de soutenances par an, y compris les années avec 0 soutenances
    all_years = pd.Series(range(df['year'].min(), df['year'].max() + 1))
    annual_counts = df['year'].value_counts().reindex(all_years, fill_value=0).sort_index()

    # Créer le premier graphique (bar chart)
    fig = go.Figure()

    fig.add_trace(go.Bar(
        x=annual_counts.index,
        y=annual_counts.values,
        text=annual_counts.values,
        textposition='auto',
        marker=dict(color=['rgb(30, 128, 133)' if i % 2 == 0 else 'rgba(0, 100, 0, 0.5)' for i in range(len(annual_counts))]),
        width=0.8
    ))

    fig.update_layout(
        title='Évolution annuelle du nombre de mémoires archivés',
        xaxis_title='Année',
        yaxis_title='Nombre de mémoires soutenues',
        xaxis=dict(
            tickmode='linear',
            tick0=annual_counts.index.min(),
            dtick=0,
            range=[annual_counts.index.min()-1, annual_counts.index.max()+1],
            ),
        plot_bgcolor='white',  # Fond blanc
        paper_bgcolor='white',  # Fond blanc
        margin=dict(l=40, r=40, t=40, b=20),  # Marges ajustées
    )

    #### FIGURE 2 ####
    parcours_counts = df['parcours'].value_counts()  # Obtenir tous les labels
    top_3_parcours = parcours_counts.nlargest(3).index.tolist()
    total_memoires = df.shape[0]

    colors = ['rgb(30, 128, 133)', 'rgba(30, 128, 133, 0.8)', 'rgba(30, 128, 133, 0.6)', 'rgba(0, 100, 0, 0.5)', 'rgba(0, 150, 0, 0.5)', 'rgba(0, 200, 0, 0.5)', 'rgba(0, 255, 0, 0.5)']

    textpositions = ['outside' if label in top_3_parcours else 'inside' for label in parcours_counts.index]

    fig2 = go.Figure(data=[go.Pie(
        labels=parcours_counts.index,
        values=parcours_counts.values,
        hole=.6,  # Pour un diagramme en anneau
        textinfo='label+percent',
        textposition=textpositions,
        insidetextorientation='radial',
        marker=dict(colors=colors)  # Utiliser les couleurs personnalisées
    )])

    fig2.update_layout(
        title_text="Répartition par filière / thématique",
        annotations=[dict(text=f'Sur {total_memoires} mémoires', x=0.5, y=0.5, font_size=16, showarrow=False)],
        showlegend=False,
        autosize=True,
        height=None,  # Laisser Dash gérer la hauteur automatiquement
        margin=dict(l=20, r=20, t=40, b=20),
    )

    #### FIGURE 3 ####
    def split_names(names):
        if isinstance(names, str):
            return names.split(' ; ')
        return names

    df['director'] = df['director'].apply(split_names)
    df['jury'] = df['jury'].apply(split_names)
    df['combined'] = df['director'] + df['jury']
    combined_members = df.explode('combined')
    combined_counts = combined_members['combined'].value_counts()
    filtered_combined_counts = combined_counts[combined_counts > 1]

    fig3 = go.Figure(go.Treemap(
        labels=filtered_combined_counts.index,
        parents=[''] * len(filtered_combined_counts),
        values=filtered_combined_counts.values,
        textinfo='label+value+percent entry',
        marker=dict(colors=filtered_combined_counts.values, colorscale='Teal'),
    ))

    fig3.update_layout(
        title="Contribution par directeur et membre du jury",
        autosize=True,
        height=None,  # Laisser Dash gérer la hauteur automatiquement
        margin=dict(l=20, r=20, t=40, b=20),
    )

    return fig, fig2, fig3
