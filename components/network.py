"""
    Master 2 Health Data Science 2022-2024
    Sujet de Webscrapping/IHM/ML - Djamel Zitouni, Wajdi Dhifli
    
    Récupération des données de Pépite - Université de Lille et les mémoires de Master 2
    d'ILIS

    Groupe composé de : 
    - Tracy LURANT
    - Malik MAMMAR
    - Hippolyte TOUZE
    - Antoine TESTON
    
    TO DO : 
    REQUIREMENT.TXT
    README.MD
    DOCSTRING des fonctions

    Fonctionnalités :
    Webscraping OK
    Interface DASH avec Dataviz OK
    NLP - BERT -> Comparaison de mots clés ? OK
    Réseaux / Kmeans ? Pas Kmeans mais réseau OK
    
    -------------------------------------------
    Script de génération du réseau des Directeurs/Jury
    
"""

#### Import Libraries ####

import pandas as pd
import plotly.graph_objects as go
from dash import Output, Input, State, html, dcc, dash
import dash_bootstrap_components as dbc
import dash_mantine_components as dmc
import dash_cytoscape as cyto
from app import app
import networkx as nx
import json
from components.datamanagement import data_management
import server
from itertools import cycle
from collections import defaultdict

from components.functions import load_graph_elements, save_graph_elements
# Load extra layouts
cyto.load_extra_layouts()

stylesheet = [
    {
        'selector': 'node',
        'style': {
            'label': 'data(label)',
            'width': 'data(size)',
            'height': 'data(size)',
            'background-color': 'data(color)',
            'font-size': '12px',
            'text-valign': 'top',
            'text-halign': 'top',
            'shape': 'ellipse',  # S'assurer que tous les nœuds sont des cercles
            'color': '#000'
        }
    },
    {
        'selector': 'node.hover',
        'style': {
            'border-color': '#000',
            'border-width': 3
        }
    },
    {
        'selector': '.director',
        'style': {
            'background-color': '#1f77b4',
            'border-width': 2,
            'border-color': '#333',
            'label': 'data(label)',
            'text-valign': 'top',
            'text-halign': 'top',
            'color': '#000'
        }
    },
    {
        'selector': '.jury',
        'style': {
            'background-color': '#ff7f0e',
            'border-width': 2,
            'border-color': '#333',
            'color': '#000'
        }
    },
    {
        'selector': 'edge',
        'style': {
            'width': 2,
            'line-color': '#888',
            'curve-style': 'bezier'
        }
    }
]


# Utility function to sanitize node IDs
def sanitize_id(node_id):
    return node_id.replace(';', '_')

@app.callback(
    Output('cytoscape-container', 'elements'),
    [Input('step', 'data')]
)
def update_graph_data(data):
    if not data or "df2" not in data:
        raise dash.exceptions.PreventUpdate

    df = pd.DataFrame(data["df2"])

    if 'director' not in df.columns or 'jury' not in df.columns:
        print("DataFrame does not contain required columns.")
        return []

    # Charger les éléments du cache si disponibles
    cached_elements = load_graph_elements()
    if cached_elements:
        return cached_elements

    # Sanitize IDs in the dataframe
    df['director'] = df['director'].apply(sanitize_id)
    df['jury'] = df['jury'].apply(lambda juries: [sanitize_id(j) for j in juries.split(' ; ')])

    # Flatten the dataframe for NetworkX
    edges = []
    for _, row in df.iterrows():
        for jury_member in row['jury']:
            edges.append((row['director'], jury_member))

    G = nx.Graph()
    G.add_edges_from(edges)

    # Ajout des attributs de nœud (spécialité, rôle, parcours)
    for i, row in df.iterrows():
        G.nodes[sanitize_id(row['director'])]['parcours'] = row['parcours']
        G.nodes[sanitize_id(row['director'])]['role'] = 'Director'
        for jury_member in row['jury']:
            G.nodes[sanitize_id(jury_member)]['parcours'] = row['parcours']
            G.nodes[sanitize_id(jury_member)]['role'] = 'Jury'

    # Assurez-vous que tous les nœuds ont un attribut 'parcours'
    for node in G.nodes():
        if 'parcours' not in G.nodes[node]:
            G.nodes[node]['parcours'] = 'unknown'  # Valeur par défaut

    # Calcul des degrés des nœuds
    degrees = dict(G.degree())

    # Normalisation de la taille des nœuds pour l'affichage
    max_degree = max(degrees.values())
    min_size = 10
    max_size = 50  # Augmenté pour une meilleure visualisation
    node_size = {node: min_size + (max_size - min_size) * (degree / max_degree) for node, degree in degrees.items()}

    # Apply a layout algorithm
    pos = nx.fruchterman_reingold_layout(G, k=5, iterations=2000, threshold=1e-10)

    # Convertir les positions pour cytoscape
    SCALING_FACTOR = 1000
    cyto_positions = {node: {'x': pos[node][0] * SCALING_FACTOR, 'y': pos[node][1] * SCALING_FACTOR} for node in G.nodes()}

    # Convertir les données pour Cytoscape
    cyto_nodes = [
        {
            'data': {
                'id': node,
                'label': node,  # Afficher les noms pour tous les nœuds
                'parcours': G.nodes[node]['parcours'],
                'role': G.nodes[node]['role'],
                'size': node_size[node]
            },
            'position': cyto_positions[node],
            'classes': 'director' if G.nodes[node]['role'] == 'Director' else 'jury'
        }
        for node in G.nodes()
    ]

    cyto_edges = [{'data': {'source': sanitize_id(edge[0]), 'target': sanitize_id(edge[1])}} for edge in G.edges()]

    elements = cyto_nodes + cyto_edges

    # Sauvegarder les éléments dans le cache
    save_graph_elements(elements)

    return elements

# Layout of the app
network = html.Div([
    cyto.Cytoscape(
        id='cytoscape-container',
        layout={'name': 'preset'},
        style={'width': '100%', 'height': '700px'},
        stylesheet=stylesheet
    ),
    html.P(id='cytoscape-tapNodeData-output'),
    html.P(id='cytoscape-tapEdgeData-output'),
    html.Div([
        html.Div([
            html.Div(style={'background-color': '#1f77b4', 'width': '20px', 'height': '20px', 'display': 'inline-block'}),
            html.Span(" Directeur", style={'margin-left': '5px'})
        ], style={'margin-bottom': '10px'}),
        html.Div([
            html.Div(style={'background-color': '#ff7f0e', 'width': '20px', 'height': '20px', 'display': 'inline-block'}),
            html.Span(" Jury", style={'margin-left': '5px'})
        ])
    ], style={'position': 'absolute', 'right': '10px', 'top': '10px', 'background-color': 'white', 'padding': '10px', 'border': '1px solid #ccc'})
])



@app.callback(Output('cytoscape-tapNodeData-output', 'children'),
              Input('cytoscape-container', 'tapNodeData'))
def displayTapNodeData(data):
    if data:
        return "Directeur de mémoire: " + data['label']


@app.callback(Output('cytoscape-tapEdgeData-output', 'children'),
              Input('cytoscape-container', 'tapEdgeData'))
def displayTapEdgeData(data):
    if data:
        return "Lien entre : " + \
               data['source'].upper() + " and " + data['target'].upper()