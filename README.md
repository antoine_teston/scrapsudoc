[![Latest Release](https://gitlab.com/antoine_teston/scrapsudoc/-/badges/release.svg)](https://gitlab.com/antoine_teston/scrapsudoc/-/releases)
[![pipeline status](https://gitlab.com/antoine_teston/scrapsudoc/badges/main/pipeline.svg)](https://gitlab.com/antoine_teston/scrapsudoc/-/commits/main)


# Pép(ai)te 
![](../assets/logo_pepite.png)

## Description 🔍

Pép(AI)te est une application Dash en Python qui extrait des données du site Pépite.univ-lille, où sont stockés les mémoires de soutenance, thèses et HDR des étudiants de l'Université de Lille. L'application permet aux utilisateurs de visualiser divers infos tels que les différents directeurs de mémoires présents durant les soutenances enregistrées, ainsi que leur implication en fonction des thématiques et des filières.

## Fonctionnalités 🛠️

- **Scraping des données** : Extraction automatique des données de mémoires depuis le site PÉPITE de l'université de Lille.
- **Visualisation des directeurs de mémoires** : Présentation des directeurs de mémoires impliqués dans les soutenances archivées.
Cette présentation fait appel à différentes techniques de traitement de texte utilisant les principes de tokenisation via `word_tokenize` de la bibliothèque `NLTK` pour diviser les mots-clés en tokens individuels.
La suppression des stops-words via `NLTK` et la lemmatisation ont permis d'obtenir le corpus puis TF-IDF via `sklearn` a été utilisé pour calculer les scores TF-IDF des mots-clés avant une représentation par wordcloud.

- **Analyse thématique et par filière** : Affichage de l'implication des directeurs de mémoires mais aussi leur implication en tant que membre de jury.
- **Analyse des soutenances de mémoires archivées** : Affiche les différents mots clés distincts par filière.

Un processus de règles d'association entre les jury et mots clés via un algorithme Apriori est en cours d'élaboration afin de permettre de prédire quel sera le meilleur directeur de mémoire en fonction des mots clés de vos travaux.

⚠️ Prérequis :

- **Avant d'installer et de lancer l'application, assurez-vous d'avoir les éléments suivants :**

- Python 3.7 ou supérieur
- pip - le gestionnaire de paquets Python
	
## Installation 📀

**Si vous souhaitez utiliser le dashboard sur votre serveur local il vous faut cloner ce dépôt GitHub et installez les dépendances nécessaires :**

```bash git clone https://gitlab.com/antoine_teston/scrapsudoc.git
	cd ScrapSudoc 
	pip install -r requirements.txt Utilisation
	python3 index.py
	```


## Structure du Projet

Voici la structure des répertoires et des fichiers du projet :

```ScrapSudoc/

	├── assets/                # Fichiers images et autres assets statiques
	│	└──	                   # logo_pepite.png
	│	└──	                   # mini_logo_pepite.png
	│	└──	                   # script.js (Animation balisage HTML)
	│	└──		               # style.css
	├── components/            # Répertoire pour les données extraites
	│   └── datamanagement.py  # Script pour l'extraction des données
	│	└── functions.py       # Référentiels de fonctions 
	│	└──	graphique.py       # Construction des différents graphes
	│	└── keywords.py        # Extraction et tendance des mots clés au cours du temps avec processus de NLP dans l'extraction de tendances
	│	└──	mainpage.py        # Layout principal avec informations générale
	│	└── network.py         # Réseau de liens entre les directeurs / jury
	│	└──	rawdata.py         # Tableau de données brute avec fonction de recherche avancées
	│	└──	sidebar.py         # Fonctions esthétiques
	├── data/                  # Fonctions utilitaires
	│   └── pepite_data.csv    # Données de scraping issues du site pépite de l'université de Lille
	├── .dockerignore          # Fichier de configuration du contenaire docker
	├── Dockerfile             # Fichier de l'image Docker 
	├── server.py              # Fichier d'application Dash
	├──	app.py                 # Fichier de lancement de l'application Dash
	├── index.py               # Script de l'architecture Dash de l'application
	├── requirements.txt       # Liste des dépendances nécessaires
	└── README.md              # Fichier de documentation
```

## Screenshots 📸

![](./assets/01.png)

![](./assets/02.png)

![](./assets/03.png)

## Built With 💻

<img src="https://user-images.githubusercontent.com/25181517/183423507-c056a6f9-1ba8-4312-a350-19bcbc5a8697.png" align="center" height="40" width="40"/> <img src="https://user-images.githubusercontent.com/25181517/117207330-263ba280-adf4-11eb-9b97-0ac5b40bc3be.png" align="center" height="40" width="40"/> <img src="https://user-images.githubusercontent.com/25181517/192108376-c675d39b-90f6-4073-bde6-5a9291644657.png " align="center" height="40" width="40"/>




## Contributions 🙋🏻‍♂🙋🏻‍♀️🙋🏼‍♂️🙋🏽‍♂️

Les contributions sont les bienvenues ! Si vous souhaitez contribuer, veuillez contacter antoine.teston.etu@univ-lille.fr :



- Antoine Teston - Data scientist - https://gitlab.com/antoine_teston
- Hippolyte Touze - Data scientist - https://gitlab.com/hippolytetouze
- Tracy Lurant - Data scientist - https://gitlab.com/L.Tracy
- Malik Mammar - Data scientist - https://gitlab.com/malikmammar


## Information ℹ️

Merci à l'Université de Lille pour leur plateforme PEPITE.
Merci à la communauté Dash et Python pour leurs bibliothèques.