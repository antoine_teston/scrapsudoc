"""
Master 2 Health Data Science 2022-2024
Sujet de Webscrapping/IHM/ML - Djamel Zitouni, Wajdi Dhifli

Récupération des données de Pépite - Université de Lille et les mémoires de Master 2
d'ILIS

Groupe composé de : 
- Tracy LURANT
- Malik MAMMAR
- Hippolyte TOUZE
- Antoine TESTON

TO DO : 
REQUIREMENT.TXT
README.MD
DOCSTRING des fonctions

Fonctionnalités :
Webscraping
Interface DASH avec Dataviz
NLP - BERT -> Comparaison de mots clés ?
Réseaux / Kmeans ? 

-------------------------------------------
Script d'embriquement des différents modules de l'application avant lancement
"""

# Import packages
import time
import dash
from dash import html, dcc, Input, Output, State
import dash_bootstrap_components as dbc
import pandas as pd
import plotly.express as px
import dash_mantine_components as dmc
import gunicorn

# Dépendance des autres scripts
from components import sidebar, mini_sidebar, mainpage, network, rawdata, keywords

from app import app, server
from server import fetch_titles_from_pepite, fetch_article_details

# Data storage elements
data = html.Div([
    dcc.Store(id='memory', storage_type='memory'),
    dcc.Store(id='step', storage_type='memory'),
    dcc.Store(id='interval-state', data={'triggered': False})
])

# Corrected content layout with grid
content = html.Div(
    id="mainContent",
    className="main-content",
    children=[
        dmc.Grid(
                dmc.Col(
                        children=[
                            dcc.Location(id='url', refresh=False),
                            html.Div(id='page-content'),
                            html.Div(
                                id="overlay",
                                className="overlay",
                                style={'display': 'none'},  # Initially hidden
                                children=[
                                    dbc.Button("×", id="close-overlay-btn", className="close-overlay-btn", color="link"),
                                    dbc.Spinner(color="info", size="lg"),
                                    dbc.Progress(id="progress-bar", striped=True, animated=True, style={"width": "50%", "margin": "20px auto"}),
                                ],
                            ),
                        ],
                    span=12
                ),
            ),
    ],
    style={"padding-top": "30px", "padding-left": "20px", "padding-right": "20px"},  # Ajout de rembourrage autour de la colonne.
    n_clicks=0
)

# Modal for pop-up message
modal = dbc.Modal(
    [
        dbc.ModalHeader(dbc.ModalTitle("Information")),
        dbc.ModalBody("Les données sont déjà à jour."),
        dbc.ModalFooter(
            dbc.Button([html.I(className="bi bi-check-circle me-2"), "Close"], id="close-modal", color="primary", className="sidebar-btn", n_clicks=0)
        ),
    ],
    id="modal",
    is_open=False,
    centered=True  # Center the modal in the window
)

# Modal for loading spinner
loading_modal = dbc.Modal(
    [
        dbc.ModalBody(
            [
                dbc.Spinner(color="primary", size="lg"),
                html.H4("Chargement des données...", className="mt-3"),
                html.Div(id="log-output", className="mt-3 text-left")  # Ajout de l'élément pour afficher les logs
            ],
            className="text-center"
        ),
    ],
    id="loading-modal",
    is_open=False,
    centered=True
)

app.layout = html.Div(
    id="app",
    children=[
        data,
        sidebar,
        mini_sidebar,
        modal,
        loading_modal,
        dcc.Interval(id='interval-component', interval=1*1000, n_intervals=0, max_intervals=5),  # Interval component to trigger loading
        content    
    ]
)

@app.callback(
    Output("page-content", "children"),
    [Input("url", "pathname")]
)
def update_page_content(pathname):
    if pathname == "/":
        return mainpage
    elif pathname == "/rawdata":
        return rawdata
    elif pathname == "/network":
        return network
    elif pathname == "/keywords":
        return keywords

    # If the user tries to reach a different page, return a 404 message
    return html.Div(
        [
            html.H1("404: Not found", className="text-danger"),
            html.Hr(),
            html.P(f"The pathname {pathname} was not recognized..."),
        ],
        className="p-3 bg-light rounded-3",
    )

if __name__ == "__main__":
    app.run_server(debug=True, host='0.0.0.0')
