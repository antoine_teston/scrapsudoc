"""
    Master 2 Health Data Science 2022-2024
    Sujet de Webscrapping/IHM/ML - Djamel Zitouni, Wajdi Dhifli
    
    Récupération des données de Pépite - Université de Lille et les mémoires de Master 2
    d'ILIS

    Groupe composé de : 
    - Tracy LURANT
    - Malik MAMMAR
    - Hippolyte TOUZE
    - Antoine TESTON
    
    TO DO : 
    REQUIREMENT.TXT
    README.MD
    DOCSTRING des fonctions

    Fonctionnalités :
    Webscraping
    Interface DASH avec Dataviz
    NLP - BERT -> Comparaison de mots clés ?
    Réseaux / Kmeans ? 
    
    -------------------------------------------
    Script de Webscrapping des données de Pépite - Université de Lille
    
"""

# Importation des packages
import os
import requests
import pandas as pd
from bs4 import BeautifulSoup
from dash import dcc, html, Dash, Input, Output
import dash_bootstrap_components as dbc
from datetime import datetime

# Constantes
BASE_URL = "https://pepite.univ-lille.fr/"
SEARCH_QUERY = "thematic-search.html?menuKey=dc&submenuKey=composante&id=ilis"
SEARCH_PATH = "ori-oai-search/"

def fetch_article_details(session, base_url, article_details_url, data, logs):
    if article_details_url.startswith('/'):
        article_details_url = base_url + article_details_url

    response = session.get(article_details_url)
    if response.status_code == 200:
        article_soup = BeautifulSoup(response.text, 'html.parser')

        title_element = article_soup.find('div', class_='titre_ressource')
        author_element = article_soup.find('div', class_='bloc_liste col-lg-12 col-md-12 col-sm-12 col-xs-12')
        keywords_element = article_soup.find('ul', class_='themes_liste_ressource')

        speciality_element = None
        director_element = None
        jury_element = None
        date_element = None
        parcours_element = None

        for ul in article_soup.find_all('ul'):
            for li in ul.find_all('li'):
                label = li.find('span', class_='mise_en_avant')
                value = li.find('span', class_='element-value')
                date_value = li.find('span', class_='date_creation')
                
                if label and value and "Discipline :" in label.text:
                    speciality_element = value.text.strip()
                elif label and value and "Directeur(s) de mémoire : " in label.text:
                    director_element = value.text.strip()
                elif label and value and "Membre(s) du jury :" in label.text:
                    jury_element = value.text.strip()
                elif label and date_value and "Date de soutenance : " in label.text:
                    date_element = date_value.text.strip()
                elif label and value and "Parcours :" in label.text:
                    parcours_element = value.text.strip()
                    break

        title = title_element.text.strip() if title_element else "No title found"
        authors = author_element.text.strip() if author_element else "No authors found"
        keywords = ", ".join(li.text.strip() for li in keywords_element.find_all('li')) if keywords_element else "No keywords found"
        speciality = speciality_element if speciality_element else "No speciality found"
        
        director = director_element if director_element else "No director found"
        jury = jury_element if jury_element else "No jury found"
        date = date_element if date_element else "No date found"
        parcours = parcours_element if parcours_element else "No parcours found"

        new_dataframe = pd.DataFrame({'title': [title],
                                      'authors': [authors],
                                      'keywords': [keywords],
                                      'speciality': [speciality],
                                      'director': [director],
                                      'jury': [jury],
                                      'date': [date],
                                      'parcours': [parcours]})

        data = pd.concat([data, new_dataframe])

        logs.append(f"Fetched details for article: {title}")
    else:
        logs.append(f"Failed to fetch article details from {article_details_url}")

    return data, logs

def fetch_titles_from_pepite(logs):
    url = f"{BASE_URL}{SEARCH_PATH}{SEARCH_QUERY}"

    session = requests.Session()
    data_complete = pd.DataFrame({'title': [], 'authors': [], 'keywords': [], 'speciality': [], 'director': [], 'jury': [], 'date': [], 'parcours': []})

    while url:
        response = session.get(url)
        if response.status_code == 200:
            soup = BeautifulSoup(response.text, 'html.parser')
            title_box = soup.find_all('div', class_='result-element result-element-title')
            if not title_box:
                break

            for box in title_box:
                title_header = box.find('a', class_='value value-title lightbox href-notice')
                if title_header and title_header.text:
                    article_details_url = BASE_URL + SEARCH_PATH + title_header['href'].strip() if title_header['href'].startswith('/') else title_header['href'].strip()
                    data_complete, logs = fetch_article_details(session, BASE_URL, article_details_url, data_complete, logs)

            logs.append("Page processed")

            next_page = soup.find('a', class_='navigation-pages refresh-page-content action-next')
            if next_page and next_page.has_attr('href'):
                next_href = next_page['href'].strip()
                url = f"{BASE_URL}{SEARCH_PATH}{next_href}"
            else:
                break
        else:
            logs.append("Failed to fetch titles")

    return data_complete, logs