document.addEventListener('DOMContentLoaded', function () {
    const closeBtn = document.getElementById('closeBtn');
    const openBtn = document.getElementById('openBtn');
    const mainSidebar = document.getElementById('mainSidebar');
    const mainContent = document.getElementById('mainContent');

    // Fonction pour redimensionner tous les graphiques Plotly
    function resizePlots() {
        var plots = document.querySelectorAll('.js-plotly-plot');
        plots.forEach(function (plot) {
            Plotly.Plots.resize(plot);
        });
    }

    // Vérifier si les éléments existent avant d'ajouter des écouteurs d'événements
    if (closeBtn && openBtn && mainSidebar && mainContent) {
        closeBtn.addEventListener('click', function () {
            mainSidebar.classList.add('closed');
            mainContent.classList.add('collapsed');
            resizePlots();  // Redimensionner les graphiques après fermeture de la sidebar
        });

        openBtn.addEventListener('click', function () {
            mainSidebar.classList.remove('closed');
            mainContent.classList.remove('collapsed');
            resizePlots();  // Redimensionner les graphiques après ouverture de la sidebar
        });
    }
});
